package CrypCurr;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
//import org.json.JSONObject;

// API-Key: bb09b898-48bd-43e8-b355-aaea23f926e9

//Klasse erzeugt
public class api {
	public static String getDayData(String coin, String datum) throws Exception {
		// Name in Kuerzel (Bitcoin -> BTC)
		if(coin.equals("bitcoin")){
			coin = "btc";	
		}
		if(coin.equals("ethereum")){
			coin = "eth";
		}
		
		// API-Link 
		String url = ("https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol="
					  + coin 
					  + "&market=EUR&apikey=QFWHGL257449PNAZ");
		
		//Seite laden und in String packen
	    InputStream is = new URL(url).openStream();
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	    // Wandelt Input von "rd" (BufferedReader) in String um
	    String jsonText = readAll(rd);
	    try {
	    	// alles aus der JSON
	    	JSONObject root = new JSONObject(jsonText);
	    	// alles aus "Time Series (Digital Currency Daily)"
	    	JSONObject timeseries = new JSONObject(root.get("Time Series (Digital Currency Daily)").toString());
	    	// alles aus der Variable "datum" (Input)
	    	JSONObject datumJ = new JSONObject(timeseries.get(datum).toString());
	    	String timeseriesS = datumJ.getString("1a. open (EUR)").toString();
	        return timeseriesS;
	      } finally {
	        is.close();
	        
	      }
	}
	
	public static String[] getWeeklyData(String coin) throws MalformedURLException, IOException {
		// Name in Kuerzel (Bitcoin -> BTC)
		// Idee: Coin-Kuerzel aus der API holen, damit sollen viele If-Schlafen wegfallen
		if(coin.equals("bitcoin")){
			coin = "btc";	
		}
		if(coin.equals("ethereum")){
			coin = "eth";
		}
		
		// API-Link 
    	String url = ("https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol="
    	+ coin 
    	+ "&market=EUR&apikey=QFWHGL257449PNAZ");
    	
    	//Seite laden und in String packen
    	InputStream is = new URL(url).openStream();
    	BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
    	String jsonText = readAll(rd);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -7);
		
		//Erstellen der Variablen
		String timeseriesS = null;
		String datumS = null;
		String all = null;
		String[] data = new String[7];
		
		
		// alles aus der JSON
    	JSONObject root = new JSONObject(jsonText);
    	// alles aus "Time Series (Digital Currency Daily)"
    	JSONObject timeseries = new JSONObject(root.get("Time Series (Digital Currency Daily)").toString());
		
		// gibt das Datum mit dem Preis aus (7mal)
		for(int i = 0; i< 7; i++){
		    cal.add(Calendar.DAY_OF_YEAR, 1);
		    datumS = sdf.format(cal.getTime());
		    
	    	// alles aus der Variable "datum" (Input)
	    	JSONObject datumJ = new JSONObject(timeseries.get(datumS).toString());
	    	timeseriesS = datumJ.getString("1a. open (EUR)").toString();
	    	
	    	
	    	all = timeseriesS + " " + datumS;
	    	data[i] = all;
	    	//System.out.println(all);
		}
		//System.out.println(Arrays.deepToString(data));
		return data;
		
	}
	
	public static String getResponse(String coin) throws IOException, InterruptedException {
		HttpRequest request1 = HttpRequest.newBuilder().uri(URI.create("https://api.coincap.io/v2/assets/"+coin)).method("GET", HttpRequest.BodyPublishers.noBody()).build();
		HttpResponse<String> response1 = HttpClient.newHttpClient().send(request1, HttpResponse.BodyHandlers.ofString());
		//int statusCode = response.statusCode();
		String body = response1.body();
		return body;
	}
	// Input: voll ausgeschrieben (statt BTC --> bitcoin)
	public static String getPrice(String coin) throws IOException, InterruptedException {
		URL url = new URL("https://api.coincap.io/v2/assets/" + coin);
		HttpURLConnection http = (HttpURLConnection)url.openConnection();
		http.setRequestProperty("Accept", "application/json");
		http.setRequestProperty("Authorization", "Bearer bb09b898-48bd-43e8-b355-aaea23f926e9");
		BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
		JSONTokener tokener = new JSONTokener(in);
	    JSONObject json = new JSONObject(tokener);
	    
	    // Hier weitermachen (aus JSONObject Preis entnehmen (und in WalletGet_METHODE packen))
	    String priceS = json.getJSONObject("data").get("priceUsd").toString();
	    System.out.println(priceS);
		return priceS;
	}
	
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	}
	
	public static String getResponseAllName() throws IOException, InterruptedException, JSONException {
		String url1 = ("https://api.coincap.io/v2/assets/");
		
		InputStream is = new URL(url1).openStream();
        try {
        	BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject data1 = new JSONObject(jsonText);
            JSONArray data2 = data1.toJSONArray(data1.names());            
            String json1 = data2.toString();
            System.out.println(json1);
            return json1;
        } finally {
        	is.close();
        }
	}
}