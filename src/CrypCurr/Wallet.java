package CrypCurr;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Wallet {
	// WalletAdd() - nur ein Asset hinzufuegen!
    public static void WalletAdd() {
    	// Erstellung des Fenster und Layout-Setzung
		JFrame walletAddPnl = new JFrame();
		walletAddPnl.setTitle("Waehrung hinzuf�gen");
		walletAddPnl.getContentPane().setLayout(new FlowLayout());
		walletAddPnl.setVisible(true);
		walletAddPnl.setSize(375, 200);
		walletAddPnl.setResizable(false);
		walletAddPnl.setLocationRelativeTo(null);
		walletAddPnl.setAlwaysOnTop(true);
						
		// Erstellung des Texteingabe-Fenster
		JTextField inputcointextwallet = new JTextField("Name", 10);
		JTextField inputmengetextwallet = new JTextField("#", 10);
		walletAddPnl.getContentPane().add(inputcointextwallet);
		walletAddPnl.getContentPane().add(inputmengetextwallet);
		
		// Hinzuf�gen der Textfelder an das JPanel
		walletAddPnl.add(inputcointextwallet);
		walletAddPnl.add(inputmengetextwallet);

		// Erstellung des Buttons und Hinzuf�gen des Button auf das Fenster
		JButton submit = new JButton("Submit");
		walletAddPnl.add(submit);
		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					// Informationsbeschaffung per String
					String coin = inputcointextwallet.getText();
					String menge1 = inputmengetextwallet.getText();
					
					try {
						DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					    
						
						File wallet = new File("wallet.xml");
						boolean exists = wallet.exists();
						
						if(exists == false) {
							System.out.println("Wallet existiert nicht");
							Document dom = builder.newDocument();
							// first create root element					    
						    Element root = dom.createElement("wallet");
						    dom.appendChild(root);
						    
						    Element asset = dom.createElement("asset");
						    root.appendChild(asset);
						    
						    // Erstellen zwei Elemente im Element Asset in Wurzel-Element (coin, menge)
						    Element coin1 = dom.createElement("coin");
						    coin1.setTextContent(coin);
						    Element menge = dom.createElement("menge");
						    menge.setTextContent(menge1);
						    
						    // add child nodes to root node
						    asset.appendChild(coin1);
						    asset.appendChild(menge);
						    
						    // Schreibe Document Object Module (DOM) in "wallet.xml"
						    Transformer tr = TransformerFactory.newInstance().newTransformer();
						    tr.setOutputProperty(OutputKeys.INDENT, "yes");
						    tr.transform(new DOMSource(dom), new StreamResult("wallet.xml"));
						}
						// Wenn "wallet.xml" exisitiert
						else {
							System.out.println("Wallet existiert");
							Document dom2 = builder.parse(wallet);
							
							// get first element				
						    Element root = dom2.getDocumentElement();
						    
						    Element asset = dom2.createElement("asset");
						    root.appendChild(asset);
						    
						    // Erstellen zwei Elemente im Element Asset in Wurzel-Element (coin, menge)
						    Element coin1 = dom2.createElement("coin");
						    coin1.setTextContent(coin);
						    Element menge = dom2.createElement("menge");
						    menge.setTextContent(menge1);
						    
						    // add child nodes to asset node
						    asset.appendChild(coin1);
						    asset.appendChild(menge);
						    
						    // write DOM to XML file
						    Transformer tr = TransformerFactory.newInstance().newTransformer();
						    tr.setOutputProperty(OutputKeys.INDENT, "yes");
						    tr.transform(new DOMSource(dom2), new StreamResult("wallet.xml"));
						}
						
					} catch (ParserConfigurationException | TransformerException | TransformerFactoryConfigurationError | SAXException | IOException e2) {
						e2.printStackTrace();
					}
			}
		});
	}
	public static void WalletGet() throws Exception {
		// Erstellung des Fenster und Festsetzung der Eigenschaften
				JFrame walletInfo = new JFrame ();
				walletInfo.setSize(750,350);
				walletInfo.setVisible(true);
				walletInfo.setResizable(true);
				walletInfo.getContentPane();
				walletInfo.setBackground(Color.black);
				walletInfo.setForeground(Color.WHITE);
				
				
				JPanel walletpnl = new JPanel();
				walletInfo.add(walletpnl);
				
				// Erstellung eines Textfelds und Festsetzung der Eigenschaften
				JTextArea wallet = new JTextArea();
				wallet.setVisible(true);
				wallet.setBackground(Color.BLACK);
				wallet.setForeground(Color.WHITE);
				
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				
				// initialize StreamResult with File object to save to file
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				File wallet2 = new File("wallet.xml");
				Document doc = builder.parse(wallet2);
		        doc.getDocumentElement().normalize();
		        
		        NodeList list = doc.getElementsByTagName("asset");
				wallet.setText("Coin 		Menge 		mome. Preis		Wert des Assets" + "\n");

		        
		        for (int temp = 0; temp < list.getLength(); temp++) {

		              Node node = list.item(temp);

		              if (node.getNodeType() == Node.ELEMENT_NODE) {

		                  Element element = (Element) node;		                  

		                  // get text
		                  String coinA = element.getElementsByTagName("coin").item(0).getTextContent();		                  
		                  String coinS = coinA;
		                  coinS = coinS.replaceAll("ADA", "cardano");
		                  coinS = coinS.replaceAll("ETH", "ethereum");
		                  coinS = coinS.replaceAll("XRP", "xrp");

		                  double price = Double.parseDouble(api.getPrice(coinS));
		               
		                  String menge = element.getElementsByTagName("menge").item(0).getTextContent();
		                  double mengeF = Double.parseDouble(menge);
		                  
		                  double priceAsset = mengeF * price;
		                 
		                  wallet.append(coinA + " 		" + mengeF + "		" + price + "\u20ac" + " 	" + priceAsset + "\u20ac" + "\n");
		              }  
		          }	
				walletInfo.setAlwaysOnTop(true);
				walletInfo.setLocationRelativeTo(null);
				walletInfo.add(wallet);
			}
	public static void WalletDel() throws ParserConfigurationException, SAXException, IOException {
				// Erstellung des Fensters und Eigenschaftenangabe
				JFrame delframe = new JFrame();
				delframe.setVisible(true);
				delframe.setSize(350,100);
				delframe.setResizable(false);
				delframe.getContentPane().setLayout(new FlowLayout());
				delframe.setAlwaysOnTop(true);
				
				// Getten der Asset-Namen und Erstellen des Drop-Down Menu 
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			    DocumentBuilder db = dbf.newDocumentBuilder();
			    Document doc = db.parse("wallet.xml");

				NodeList assets = doc.getElementsByTagName("asset");
				int a = assets.getLength();
				String[] xmlAssets = new String[a];
				
				/*
				fuer jeden Zahlenschritt, der unter der Laenge der NodeList ist,
				soll der Asset-Name in ein String gepackt werden und in das Array hinzugefuegt werden
				*/ 
				for(int i = 0; i < assets.getLength(); i++) {
					Element asset = (Element)assets.item(i);
				    Element name = (Element)asset.getElementsByTagName("coin").item(0);
				    String assetName = name.getTextContent();
				    xmlAssets[i] = assetName;
				}
				// Erstellen der JComboBox und Hinzufügen im Fenster "delframe"
				JComboBox<?> assetsJBox = new JComboBox(xmlAssets);
				delframe.getContentPane().add(assetsJBox);
			
				// TextFeld DelMenge
				JTextField delmengefield = new JTextField("#",10);
				delframe.add(delmengefield);
				
				JButton delsubmit = new JButton("Loeschen");
				delsubmit.setVisible(true);
				delframe.add(delsubmit);
			
				delsubmit.addActionListener(new ActionListener()  {
					public void actionPerformed(ActionEvent e) {
						try {
							// Getten der eingebenden Menge und Coins
							String delcoin = String.valueOf(assetsJBox.getSelectedItem());
							String mengeweg = delmengefield.getText();
							
							Double mengewegD = Double.valueOf(mengeweg);
							System.out.println(mengewegD);
							
							NodeList nodes = doc.getElementsByTagName("asset");
							
							// solange i nicht der Laenge der Nodes entspricht mach:
						    for (int i = 0; i < nodes.getLength(); i++) {
						      Element asset = (Element)nodes.item(i);
						      
						      // Getten der Elemente des Wallets (Name, Menge)
						      Element name = (Element)asset.getElementsByTagName("coin").item(0);
						      Element menge = (Element)asset.getElementsByTagName("menge").item(0);
						      
						      // Wert-Abgabe an String "pName"
						      String pName = name.getTextContent();
						      
						      // wenn eingegebener Coin dem in der Wallet-Liste ("wallet.xml") entspricht
						      if (pName.equals(delcoin)) {
						    	  if(mengewegD == 0) {
						    		  System.out.println("Asset wird geloescht!");
						    		  asset.getParentNode().removeChild(asset);
						    		  
						    		  TransformerFactory tf = TransformerFactory.newInstance();
								      Transformer t = tf.newTransformer();
								      t.transform(new DOMSource(doc), new StreamResult("wallet.xml"));
						    	  } else {
						    		  System.out.println("Asset wird bearbeitet");
						    		  // Getten des Wertes "Menge" aus der "wallet.xml" 
						    		  String pMenge = menge.getTextContent();
						    		  // Umwandeln in ein Integer
						    		  Double walletmenge = Double.valueOf(pMenge);
						    		  System.out.println(walletmenge);
									  // Rechnenoperation
						    		  Double newMenge = walletmenge - mengewegD;
						    		  System.out.println(newMenge);
						    		  
						    		  // Ändern der Menge in XML-Datei
						    		  System.out.println(String.valueOf(newMenge));
						    		  menge.setTextContent(String.valueOf(newMenge));
								    	 
								      
								      TransformerFactory tf = TransformerFactory.newInstance();
								      Transformer t = tf.newTransformer();
								      t.transform(new DOMSource(doc), new StreamResult("wallet.xml"));
						    	  }
						      }
						    }					
						} 
						//Exception-Block
						catch (NumberFormatException | TransformerException e1) {
							e1.printStackTrace();
						}
					}
				});	
				delframe.setAlwaysOnTop(true);
				delframe.setLocationRelativeTo(null);
			}
}