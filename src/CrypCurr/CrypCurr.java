package CrypCurr;
import javax.swing.*;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JTextArea;
import javax.xml.parsers.ParserConfigurationException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.json.JSONException;
import org.xml.sax.SAXException;

import javax.json.*;


public class CrypCurr  {
	// METHOD countLinesNew
	public static int countLinesNew(String filename) throws IOException {
	    InputStream is = new BufferedInputStream(new FileInputStream(filename));
	    try {
	        byte[] c = new byte[1024];

	        int readChars = is.read(c);
	        if (readChars == -1) {
	            return 0;
	        }

	        int count = 0;
	        while (readChars == 1024) {
	            for (int i=0; i<1024;) {
	                if (c[i++] == '\n') {
	                    ++count;
	                }
	            }
	            readChars = is.read(c);
	        }
	        while (readChars != -1) {
	            for (int i=0; i<readChars; ++i) {
	                if (c[i] == '\n') {
	                    ++count;
	                }
	            }
	            readChars = is.read(c);
	        }

	        return count == 0 ? 1 : count;
	    } finally {
	        is.close();
	    }
	}
	// METHOD Diagramm	
	private static void DiagrammTest(String coin) throws Exception{
		// Erstellen des GUIs und der Eigenschaften
		JDialog Diagramm = new JDialog();
        Diagramm.setTitle("Diagramm");
        Diagramm.setSize(1125,850);
        Diagramm.setVisible(true);
        Diagramm.setResizable(true);
		Diagramm.setLocationRelativeTo(null);
		Diagramm.setAlwaysOnTop(true);
		
				
		coin = "dot";
		String[] dataSA = api.getWeeklyData(coin);
		
		double [][] data = new double[2][7];

		//System.out.println(Arrays.deepToString(dataSA));
		
		for(int i = 0; i < 7; i++) {
			String cur = dataSA[i];
			cur = cur.replace("-", "");
			String[] curB = cur.split(" ");
			double price = Double.parseDouble(curB[0]);
			int datum = Integer.parseInt(curB[1]);
			data[1][i] = price;
			data[0][i] = i;
		}
		DefaultXYDataset ds = new DefaultXYDataset();
		
		ds.addSeries("xy", data);
		
		JFreeChart chart = ChartFactory.createXYAreaChart("Preis von  " + coin, "Datum", "Preis", ds, PlotOrientation.VERTICAL, true, true, false);
		ChartPanel cp = new ChartPanel(chart);
		chart.getXYPlot().setRenderer(new XYSplineRenderer());
		Diagramm.add(cp);
		Diagramm.getContentPane().setLayout(new FlowLayout());
	}
	
	// METHOD PieDiagrammTest (testing) (skalieren um zu sehen) (not activated) 
	/*
		private static void PortfolioDiagramm(){
			JDialog Diagramm = new JDialog();
	        Diagramm.setTitle("Input");
	        Diagramm.setSize(750,450);
	        Diagramm.setVisible(true);
	        Diagramm.setResizable(true);
			Diagramm.setLocationRelativeTo(null);
			Diagramm.setAlwaysOnTop(true);
			Diagramm.getContentPane().setLayout(new FlowLayout());

			DefaultPieDataset<String> ds = new DefaultPieDataset<String>();
			ds.setValue("Linux", 29);
			ds.setValue("Windows", 59);
			ds.setValue("OSX", 2);

			JFreeChart chart = ChartFactory.createPieChart("Test", ds, ds, 0, false, false, false, true, true, false);
			ChartPanel cp = new ChartPanel(chart);
			Diagramm.add(cp);
		}
	*/
	
	// METHOD Coin100 (partly working; searching expected)
	public static void Coin100() throws IOException, InterruptedException, JsonException {
		
		// TextFeld Coin100-Liste
		JTextArea coin100a = new JTextArea(59,26);
		coin100a.setVisible(true);
		coin100a.setBackground(Color.BLACK);
		coin100a.setForeground(Color.WHITE);
		coin100a.setLineWrap(true);
		
		// Erstellung des Fenster und Einstellen der Eigenschaften
		JFrame coin100 = new JFrame();
		coin100.setResizable(false);
		coin100.setLocationRelativeTo(null);
		coin100.setAlwaysOnTop(true);
		coin100.setVisible(true);
		coin100.setBounds(0,0,325,1000);
		coin100.getContentPane();
	    coin100.setLocationRelativeTo(null);
		coin100.add(coin100a);
		
		JScrollPane scrollPane = new JScrollPane(coin100a);
		coin100.getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
		coin100.getContentPane().setLayout(new FlowLayout());
		
		
			// RAW-Input der API verarbeiten (unnoetige Zeichen entfernen)
			String coin100old;
			coin100old = api.getResponseAllName();
			coin100old = coin100old.replaceAll("\"", "");
			coin100old = coin100old.replaceAll(",", "\n");
			coin100old = coin100old.replaceAll("\\{", "\n");
			coin100old = coin100old.replaceAll("}", "\n");
			
			//coin100old = coin100old.replaceAll(".000", " ");
			coin100old = coin100old.replaceAll(",maxSupply:null", " ");
			coin100old = coin100old.replaceAll("symbol:", " ");
			
			coin100a.append(coin100old);
			coin100a.setCaretPosition(0); 
	}
	// METHOD InfoFenster (working)
	@SuppressWarnings("rawtypes")
	private static void InfoFenster() throws IOException, InterruptedException{
		// Erstellung des Fenster und Layout-Setzung
		JFrame inputcoinfenster = new JFrame();
		inputcoinfenster.setResizable(false);
		inputcoinfenster.setLocationRelativeTo(null);
		inputcoinfenster.setAlwaysOnTop(true);
		
		// TextFeld f�r alternative Eingabe
		JTextField othercoin = new JTextField("", 15);
		othercoin.setForeground(Color.WHITE);
		othercoin.setBackground(Color.BLACK);
		JButton othercoinb = new JButton("Ok");
		inputcoinfenster.add(othercoin);
		inputcoinfenster.add(othercoinb);
		inputcoinfenster.getContentPane()
						.setLayout(new FlowLayout());
		
		// wenn alternative Eingabe best�tigen werden soll
		othercoinb.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent e) {
				try {
					String othercoinS = othercoin.getText();
					othercoinS = othercoinS.toLowerCase();	
					
					JDialog PanelInfo = new JDialog ();
					String inputcoinTitle = othercoinS.substring(0, 1).toUpperCase() + othercoinS.substring(1);
					PanelInfo.setTitle(inputcoinTitle);
					PanelInfo.setSize(217,500);
					PanelInfo.setVisible(true);
					PanelInfo.setLayout(null);
					PanelInfo.setResizable(false);
					JTextArea ticker = new JTextArea();
					ticker.setBounds(0, 0, 200, 500);
					ticker.setBackground(Color.BLACK);
					ticker.setForeground(Color.WHITE);
					
					PanelInfo.add(ticker);

					// Response-Anfrage an API-Klasse
					String responseOld = api.getResponse(othercoinS);
					// Auswechseln von Gaensefuessen gegen VOID und Kommata gegen Zeilenumbruch
					responseOld.replaceAll("\"", "");
					responseOld = responseOld.replaceAll(",", "\n");
					ticker.append(responseOld);
					ticker.setLineWrap(true);
					// Setzen der Sichtbarkeit
					ticker.setVisible(true);
					inputcoinfenster.dispose();
					
					
				} catch (IOException | InterruptedException | JSONException e1) {
					e1.printStackTrace();
				}
			}
		});	
							
		// Liste der Coins
		String coins[] = {"Bitcoin", "Ethereum", "Litecoin", "XRP", "Polkadot", "Uniswap", "Binance-Coin", "Cardano"};
		
		@SuppressWarnings("unchecked")
		// Erstellen eines Drop-Down-Menue
		JComboBox inputcointext = new JComboBox(coins);
		inputcoinfenster.getContentPane().add(inputcointext);
		
		inputcoinfenster.add(inputcointext); 

		// Erstellung des Buttons und Hinzufuegen des Button auf das Fenster
		JButton submit = new JButton("Ok");
		// Achten auf Aktionen (Submit-Knopf)
		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String inputcoin = String.valueOf(inputcointext.getSelectedItem());
					inputcoin = inputcoin.toLowerCase();					
					
					JDialog PanelInfo = new JDialog ();
					String inputcoinTitle = inputcoin.substring(0, 1).toUpperCase() + inputcoin.substring(1);
					PanelInfo.setTitle(inputcoinTitle);
					PanelInfo.setSize(217,500);
					PanelInfo.setVisible(true);
					PanelInfo.setLayout(null);
					PanelInfo.setResizable(false);
					JTextArea ticker = new JTextArea();
					ticker.setBounds(0, 0, 200, 500);
					ticker.setBackground(Color.BLACK);
					ticker.setForeground(Color.WHITE);
					PanelInfo.add(ticker);

					// Response-Anfrage an API-Klasse
			        String responseOld = api.getResponse(inputcoin);
					// Auswechseln von Gaensefuessen gegen VOID und Kommata gegen Zeilenumbruch
					String responseNew = responseOld.replaceAll("\"", "");
					responseNew = responseNew.replaceAll(",", "\n");
					ticker.append(responseNew);
					ticker.setLineWrap(true);
					// Setzen der Sichtbarkeit
					ticker.setVisible(true);
					inputcoinfenster.dispose();
					
					
				} catch (IOException | InterruptedException | JSONException e1) {
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		inputcoinfenster.add(submit);
        inputcoinfenster.pack();
        inputcoinfenster.setVisible(true);
        inputcoinfenster.setLocationRelativeTo(null);
	}
	
	public static void DiagramData() {
		
	}
	
	public static void Data() throws Exception {
		// Erstellung des Fenster und Layout-Setzung
		JFrame data = new JFrame();
		data.setResizable(true);
		data.setLocationRelativeTo(null);
		data.setAlwaysOnTop(true);
		data.setBounds(0,0,325,1000);
		
		JTextField datumF = new JTextField("Datum", 15);
		datumF.setForeground(Color.WHITE);
		datumF.setBackground(Color.BLACK);
		
		JTextField coin = new JTextField("Coin", 15);
		coin.setForeground(Color.WHITE);
		coin.setBackground(Color.BLACK);
		
		JButton datumButton = new JButton("Ok");
		
		data.add(coin);
		data.add(datumF);
		data.add(datumButton);
		data.getContentPane()
			.setLayout(new FlowLayout());
		data.pack();
		data.setVisible(true);
		data.setLocationRelativeTo(null);
		
		datumButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String coin1 = coin.getText();
					String datum = datumF.getText();
					String data1 = api.getDayData(coin1, datum);
					System.out.println(data1);
					
					JTextArea ticker = new JTextArea();
					ticker.setBounds(0, 0, 200, 500);
					ticker.setBackground(Color.BLACK);
					ticker.setForeground(Color.WHITE);
					data.getContentPane()
					.setLayout(new FlowLayout());
					
					ticker.append(data1);
					data.add(ticker);
					data.add(datumF);
					
				} catch (JSONException e1) {
					e1.printStackTrace();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	
	// Main-Methode
	public static void main(String[] args) throws Exception {   
		// Erstellen des Fensters, Festlegung des Namens und Groesse
		JDialog fenster = new JDialog();
		fenster.setTitle("CrypCurr");			
		fenster.setSize(500,250);
		fenster.setResizable(false);

				
		// Erzeugung der JPanels
		JPanel pnl1 = new JPanel();
		JPanel pnl2 = new JPanel();
						
		// Erzeugung von Knoepfen im ersten Tab
		JButton Info = new JButton("Coin-Ticker");
		JButton Coin100 = new JButton("Coin100");
		JButton Data = new JButton("Zeit-Daten");
		
		// zweiter Tab
		JButton walletAdd = new JButton("Coin hinzufuegen");
		JButton walletGet = new JButton("Wallet anzeigen");
		JButton walletDel = new JButton("Asset loeschen");
				
		JButton Diagramm = new JButton("Diagramm");
		
		// Achten auf Aktionen (Data)
		Data.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
						Data();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		        }
		});
		
		// Achten auf Aktionen (Coin100)
		Coin100.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent e) {
				try {
					Coin100();
				} catch (IOException | InterruptedException | JSONException e1) {
					e1.printStackTrace();
				}
			}
		});
				
		// Achten auf Aktionen (InfoFenster)
		Info.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent e) {
				try {
					InfoFenster();
				} catch (IOException | InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		// Achten auf Aktionen (WalletAdd)
		walletAdd.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent e)  {
				Wallet.WalletAdd();
				}
		});

		// Achten auf Aktionen (WalletGet)
		walletGet.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Wallet.WalletGet();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				}
		});
		
		// Achten auf Aktionen (WalletCal)
		walletDel.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent e) {
				try {
					Wallet.WalletDel();
				} catch (ParserConfigurationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
		});
		
		// Achten auf Aktionen (Diagramm)
				Diagramm.addActionListener(new ActionListener()  {
					public void actionPerformed(ActionEvent e) {
							try {
								DiagrammTest("btc");
								//PieDiagrammTest();
							} catch (NumberFormatException e1) {
								e1.printStackTrace();
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
				});
			
		fenster.getContentPane().add(Data);
		fenster.getContentPane().add(Info);
		fenster.getContentPane().add(walletAdd);
		fenster.getContentPane().add(walletGet);
		fenster.getContentPane().add(walletDel);
		fenster.getContentPane().add(Diagramm);
				
		//Informationssetzung (Knopf) der JPanels
		pnl1.add(new JLabel("Hallo Welt!"));
		//pnl2.add(new JLabel("Hallo Welt...nochmal!"));
		pnl1.add((Diagramm));
		pnl1.add(Data);
		pnl1.add((Info));
		pnl1.add((Coin100));
		pnl2.add((walletAdd));
		pnl2.add((walletGet));
		pnl2.add((walletDel));
		
        // Erzeugung eines JTabbedPane-Objektes
        JTabbedPane tabpane = new JTabbedPane(JTabbedPane.LEFT,JTabbedPane.SCROLL_TAB_LAYOUT );
        
        // Die JPanels werden als Registerkarten hinzugef�gt
        tabpane.addTab("Cointicker", pnl1);
        tabpane.addTab("Wallet", pnl2);
        
        // JTabbedPane wird zum Dialog hinzugef�gt
        fenster.add(tabpane);
        
        // Sichtbarkeit des Fensters
        fenster.setVisible(true);  
        
        fenster.setLocationRelativeTo(null);
        
        fenster.setAlwaysOnTop(true);
        
	}
}